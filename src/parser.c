#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 13
#define STATE_COUNT 12
#define LARGE_STATE_COUNT 10
#define SYMBOL_COUNT 22
#define ALIAS_COUNT 0
#define TOKEN_COUNT 16
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 0
#define MAX_ALIAS_SEQUENCE_LENGTH 3
#define PRODUCTION_ID_COUNT 1

enum {
  anon_sym_POUND = 1,
  aux_sym_comment_token1 = 2,
  sym_addition_function = 3,
  sym_subtraction_function = 4,
  anon_sym_0 = 5,
  anon_sym_1 = 6,
  anon_sym_2 = 7,
  anon_sym_3 = 8,
  anon_sym_4 = 9,
  anon_sym_5 = 10,
  anon_sym_6 = 11,
  anon_sym_7 = 12,
  anon_sym_8 = 13,
  anon_sym_9 = 14,
  anon_sym_DOT = 15,
  sym_source_file = 16,
  sym_comment = 17,
  sym_expression = 18,
  sym_church_numeral = 19,
  sym_application = 20,
  aux_sym_source_file_repeat1 = 21,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [anon_sym_POUND] = "#",
  [aux_sym_comment_token1] = "comment_token1",
  [sym_addition_function] = "addition_function",
  [sym_subtraction_function] = "subtraction_function",
  [anon_sym_0] = "0",
  [anon_sym_1] = "1",
  [anon_sym_2] = "2",
  [anon_sym_3] = "3",
  [anon_sym_4] = "4",
  [anon_sym_5] = "5",
  [anon_sym_6] = "6",
  [anon_sym_7] = "7",
  [anon_sym_8] = "8",
  [anon_sym_9] = "9",
  [anon_sym_DOT] = ".",
  [sym_source_file] = "source_file",
  [sym_comment] = "comment",
  [sym_expression] = "expression",
  [sym_church_numeral] = "church_numeral",
  [sym_application] = "application",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [anon_sym_POUND] = anon_sym_POUND,
  [aux_sym_comment_token1] = aux_sym_comment_token1,
  [sym_addition_function] = sym_addition_function,
  [sym_subtraction_function] = sym_subtraction_function,
  [anon_sym_0] = anon_sym_0,
  [anon_sym_1] = anon_sym_1,
  [anon_sym_2] = anon_sym_2,
  [anon_sym_3] = anon_sym_3,
  [anon_sym_4] = anon_sym_4,
  [anon_sym_5] = anon_sym_5,
  [anon_sym_6] = anon_sym_6,
  [anon_sym_7] = anon_sym_7,
  [anon_sym_8] = anon_sym_8,
  [anon_sym_9] = anon_sym_9,
  [anon_sym_DOT] = anon_sym_DOT,
  [sym_source_file] = sym_source_file,
  [sym_comment] = sym_comment,
  [sym_expression] = sym_expression,
  [sym_church_numeral] = sym_church_numeral,
  [sym_application] = sym_application,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_POUND] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_comment_token1] = {
    .visible = false,
    .named = false,
  },
  [sym_addition_function] = {
    .visible = true,
    .named = true,
  },
  [sym_subtraction_function] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_0] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_1] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_2] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_3] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_4] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_5] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_6] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_7] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_8] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_9] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_church_numeral] = {
    .visible = true,
    .named = true,
  },
  [sym_application] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(4);
      if (lookahead == '#') ADVANCE(5);
      if (lookahead == '+') ADVANCE(8);
      if (lookahead == '-') ADVANCE(9);
      if (lookahead == '.') ADVANCE(20);
      if (lookahead == '0') ADVANCE(10);
      if (lookahead == '1') ADVANCE(11);
      if (lookahead == '2') ADVANCE(12);
      if (lookahead == '3') ADVANCE(13);
      if (lookahead == '4') ADVANCE(14);
      if (lookahead == '5') ADVANCE(15);
      if (lookahead == '6') ADVANCE(16);
      if (lookahead == '7') ADVANCE(17);
      if (lookahead == '8') ADVANCE(18);
      if (lookahead == '9') ADVANCE(19);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(0)
      END_STATE();
    case 1:
      if (lookahead == '\n') SKIP(1)
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(2);
      if (lookahead != 0) ADVANCE(3);
      END_STATE();
    case 2:
      if (lookahead == '\n') ADVANCE(7);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(2);
      if (lookahead != 0) ADVANCE(3);
      END_STATE();
    case 3:
      if (lookahead == '\n') ADVANCE(6);
      if (lookahead != 0) ADVANCE(3);
      END_STATE();
    case 4:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 5:
      ACCEPT_TOKEN(anon_sym_POUND);
      END_STATE();
    case 6:
      ACCEPT_TOKEN(aux_sym_comment_token1);
      END_STATE();
    case 7:
      ACCEPT_TOKEN(aux_sym_comment_token1);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(2);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(3);
      END_STATE();
    case 8:
      ACCEPT_TOKEN(sym_addition_function);
      END_STATE();
    case 9:
      ACCEPT_TOKEN(sym_subtraction_function);
      END_STATE();
    case 10:
      ACCEPT_TOKEN(anon_sym_0);
      END_STATE();
    case 11:
      ACCEPT_TOKEN(anon_sym_1);
      END_STATE();
    case 12:
      ACCEPT_TOKEN(anon_sym_2);
      END_STATE();
    case 13:
      ACCEPT_TOKEN(anon_sym_3);
      END_STATE();
    case 14:
      ACCEPT_TOKEN(anon_sym_4);
      END_STATE();
    case 15:
      ACCEPT_TOKEN(anon_sym_5);
      END_STATE();
    case 16:
      ACCEPT_TOKEN(anon_sym_6);
      END_STATE();
    case 17:
      ACCEPT_TOKEN(anon_sym_7);
      END_STATE();
    case 18:
      ACCEPT_TOKEN(anon_sym_8);
      END_STATE();
    case 19:
      ACCEPT_TOKEN(anon_sym_9);
      END_STATE();
    case 20:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 0},
  [2] = {.lex_state = 0},
  [3] = {.lex_state = 0},
  [4] = {.lex_state = 0},
  [5] = {.lex_state = 0},
  [6] = {.lex_state = 0},
  [7] = {.lex_state = 0},
  [8] = {.lex_state = 0},
  [9] = {.lex_state = 0},
  [10] = {.lex_state = 1},
  [11] = {.lex_state = 0},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_POUND] = ACTIONS(1),
    [sym_addition_function] = ACTIONS(1),
    [sym_subtraction_function] = ACTIONS(1),
    [anon_sym_0] = ACTIONS(1),
    [anon_sym_1] = ACTIONS(1),
    [anon_sym_2] = ACTIONS(1),
    [anon_sym_3] = ACTIONS(1),
    [anon_sym_4] = ACTIONS(1),
    [anon_sym_5] = ACTIONS(1),
    [anon_sym_6] = ACTIONS(1),
    [anon_sym_7] = ACTIONS(1),
    [anon_sym_8] = ACTIONS(1),
    [anon_sym_9] = ACTIONS(1),
    [anon_sym_DOT] = ACTIONS(1),
  },
  [1] = {
    [sym_source_file] = STATE(11),
    [sym_comment] = STATE(2),
    [sym_expression] = STATE(4),
    [sym_church_numeral] = STATE(6),
    [sym_application] = STATE(6),
    [aux_sym_source_file_repeat1] = STATE(2),
    [anon_sym_POUND] = ACTIONS(3),
    [sym_addition_function] = ACTIONS(5),
    [sym_subtraction_function] = ACTIONS(5),
    [anon_sym_0] = ACTIONS(7),
    [anon_sym_1] = ACTIONS(7),
    [anon_sym_2] = ACTIONS(7),
    [anon_sym_3] = ACTIONS(7),
    [anon_sym_4] = ACTIONS(7),
    [anon_sym_5] = ACTIONS(7),
    [anon_sym_6] = ACTIONS(7),
    [anon_sym_7] = ACTIONS(7),
    [anon_sym_8] = ACTIONS(7),
    [anon_sym_9] = ACTIONS(7),
  },
  [2] = {
    [sym_comment] = STATE(3),
    [sym_expression] = STATE(4),
    [sym_church_numeral] = STATE(6),
    [sym_application] = STATE(6),
    [aux_sym_source_file_repeat1] = STATE(3),
    [ts_builtin_sym_end] = ACTIONS(9),
    [anon_sym_POUND] = ACTIONS(3),
    [sym_addition_function] = ACTIONS(5),
    [sym_subtraction_function] = ACTIONS(5),
    [anon_sym_0] = ACTIONS(7),
    [anon_sym_1] = ACTIONS(7),
    [anon_sym_2] = ACTIONS(7),
    [anon_sym_3] = ACTIONS(7),
    [anon_sym_4] = ACTIONS(7),
    [anon_sym_5] = ACTIONS(7),
    [anon_sym_6] = ACTIONS(7),
    [anon_sym_7] = ACTIONS(7),
    [anon_sym_8] = ACTIONS(7),
    [anon_sym_9] = ACTIONS(7),
  },
  [3] = {
    [sym_comment] = STATE(3),
    [sym_expression] = STATE(4),
    [sym_church_numeral] = STATE(6),
    [sym_application] = STATE(6),
    [aux_sym_source_file_repeat1] = STATE(3),
    [ts_builtin_sym_end] = ACTIONS(11),
    [anon_sym_POUND] = ACTIONS(13),
    [sym_addition_function] = ACTIONS(16),
    [sym_subtraction_function] = ACTIONS(16),
    [anon_sym_0] = ACTIONS(19),
    [anon_sym_1] = ACTIONS(19),
    [anon_sym_2] = ACTIONS(19),
    [anon_sym_3] = ACTIONS(19),
    [anon_sym_4] = ACTIONS(19),
    [anon_sym_5] = ACTIONS(19),
    [anon_sym_6] = ACTIONS(19),
    [anon_sym_7] = ACTIONS(19),
    [anon_sym_8] = ACTIONS(19),
    [anon_sym_9] = ACTIONS(19),
  },
  [4] = {
    [sym_expression] = STATE(5),
    [sym_church_numeral] = STATE(6),
    [sym_application] = STATE(6),
    [ts_builtin_sym_end] = ACTIONS(22),
    [anon_sym_POUND] = ACTIONS(22),
    [sym_addition_function] = ACTIONS(5),
    [sym_subtraction_function] = ACTIONS(5),
    [anon_sym_0] = ACTIONS(7),
    [anon_sym_1] = ACTIONS(7),
    [anon_sym_2] = ACTIONS(7),
    [anon_sym_3] = ACTIONS(7),
    [anon_sym_4] = ACTIONS(7),
    [anon_sym_5] = ACTIONS(7),
    [anon_sym_6] = ACTIONS(7),
    [anon_sym_7] = ACTIONS(7),
    [anon_sym_8] = ACTIONS(7),
    [anon_sym_9] = ACTIONS(7),
  },
  [5] = {
    [sym_expression] = STATE(5),
    [sym_church_numeral] = STATE(6),
    [sym_application] = STATE(6),
    [sym_addition_function] = ACTIONS(5),
    [sym_subtraction_function] = ACTIONS(5),
    [anon_sym_0] = ACTIONS(7),
    [anon_sym_1] = ACTIONS(7),
    [anon_sym_2] = ACTIONS(7),
    [anon_sym_3] = ACTIONS(7),
    [anon_sym_4] = ACTIONS(7),
    [anon_sym_5] = ACTIONS(7),
    [anon_sym_6] = ACTIONS(7),
    [anon_sym_7] = ACTIONS(7),
    [anon_sym_8] = ACTIONS(7),
    [anon_sym_9] = ACTIONS(7),
    [anon_sym_DOT] = ACTIONS(24),
  },
  [6] = {
    [ts_builtin_sym_end] = ACTIONS(26),
    [anon_sym_POUND] = ACTIONS(26),
    [sym_addition_function] = ACTIONS(26),
    [sym_subtraction_function] = ACTIONS(26),
    [anon_sym_0] = ACTIONS(26),
    [anon_sym_1] = ACTIONS(26),
    [anon_sym_2] = ACTIONS(26),
    [anon_sym_3] = ACTIONS(26),
    [anon_sym_4] = ACTIONS(26),
    [anon_sym_5] = ACTIONS(26),
    [anon_sym_6] = ACTIONS(26),
    [anon_sym_7] = ACTIONS(26),
    [anon_sym_8] = ACTIONS(26),
    [anon_sym_9] = ACTIONS(26),
    [anon_sym_DOT] = ACTIONS(26),
  },
  [7] = {
    [ts_builtin_sym_end] = ACTIONS(28),
    [anon_sym_POUND] = ACTIONS(28),
    [sym_addition_function] = ACTIONS(28),
    [sym_subtraction_function] = ACTIONS(28),
    [anon_sym_0] = ACTIONS(28),
    [anon_sym_1] = ACTIONS(28),
    [anon_sym_2] = ACTIONS(28),
    [anon_sym_3] = ACTIONS(28),
    [anon_sym_4] = ACTIONS(28),
    [anon_sym_5] = ACTIONS(28),
    [anon_sym_6] = ACTIONS(28),
    [anon_sym_7] = ACTIONS(28),
    [anon_sym_8] = ACTIONS(28),
    [anon_sym_9] = ACTIONS(28),
    [anon_sym_DOT] = ACTIONS(28),
  },
  [8] = {
    [ts_builtin_sym_end] = ACTIONS(30),
    [anon_sym_POUND] = ACTIONS(30),
    [sym_addition_function] = ACTIONS(30),
    [sym_subtraction_function] = ACTIONS(30),
    [anon_sym_0] = ACTIONS(30),
    [anon_sym_1] = ACTIONS(30),
    [anon_sym_2] = ACTIONS(30),
    [anon_sym_3] = ACTIONS(30),
    [anon_sym_4] = ACTIONS(30),
    [anon_sym_5] = ACTIONS(30),
    [anon_sym_6] = ACTIONS(30),
    [anon_sym_7] = ACTIONS(30),
    [anon_sym_8] = ACTIONS(30),
    [anon_sym_9] = ACTIONS(30),
    [anon_sym_DOT] = ACTIONS(30),
  },
  [9] = {
    [ts_builtin_sym_end] = ACTIONS(32),
    [anon_sym_POUND] = ACTIONS(32),
    [sym_addition_function] = ACTIONS(32),
    [sym_subtraction_function] = ACTIONS(32),
    [anon_sym_0] = ACTIONS(32),
    [anon_sym_1] = ACTIONS(32),
    [anon_sym_2] = ACTIONS(32),
    [anon_sym_3] = ACTIONS(32),
    [anon_sym_4] = ACTIONS(32),
    [anon_sym_5] = ACTIONS(32),
    [anon_sym_6] = ACTIONS(32),
    [anon_sym_7] = ACTIONS(32),
    [anon_sym_8] = ACTIONS(32),
    [anon_sym_9] = ACTIONS(32),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 1,
    ACTIONS(34), 1,
      aux_sym_comment_token1,
  [4] = 1,
    ACTIONS(36), 1,
      ts_builtin_sym_end,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(10)] = 0,
  [SMALL_STATE(11)] = 4,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [5] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT(7),
  [9] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1),
  [11] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [13] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(10),
  [16] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(6),
  [19] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(7),
  [22] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 1),
  [24] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [26] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_expression, 1),
  [28] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_church_numeral, 1),
  [30] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_application, 3),
  [32] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comment, 2),
  [34] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [36] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_worse(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
