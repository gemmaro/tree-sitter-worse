# tree-sitter-worse

Tree-sitter for [the Worse programming language][worse].

[worse]: https://github.com/unsigned-wrong-wrong-int/worse-lang

## License

Some test cases are taken from original Worse repository,
and [they are distributed under the MIT license][mit].

[mit]: https://github.com/unsigned-wrong-wrong-int/worse-lang/blob/main/LICENSE

Here are the references:

* helloworld: [`samples/helloworld.worse`][hello]
* cat: [`samples/cat.worse`][cat]

[hello]: https://github.com/unsigned-wrong-wrong-int/worse-lang/blob/main/samples/helloworld.worse
[cat]: https://github.com/unsigned-wrong-wrong-int/worse-lang/blob/main/samples/cat.worse

[So does this parser](LICENSE.txt).
