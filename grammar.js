module.exports = grammar({
  name: "worse",
  rules: {
    source_file: ($) => repeat1(prec.right(choice($.comment, $.expression))),
    comment: (_$) => seq("#", /.+\n/),
    expression: ($) =>
      choice(
        $.addition_function,
        $.subtraction_function,
        $.church_numeral,
        $.application,
      ),
    addition_function: (_$) => "+",
    subtraction_function: (_$) => "-",
    church_numeral: (_$) =>
      choice("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"),
    application: ($) => seq($.expression, $.expression, "."),
  },
});
